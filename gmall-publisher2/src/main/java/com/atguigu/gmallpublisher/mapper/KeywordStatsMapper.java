package com.atguigu.gmallpublisher.mapper;


import com.atguigu.gmallpublisher.bean.KeywordStats;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface KeywordStatsMapper {

    @Select("select keyword,sum(keyword_count) ct from dws_traffic_source_keyword_page_view_window where toYYYYMMDD(stt)=#{date} group by keyword order by ct desc limit #{limit}")
    public List<KeywordStats> selectKeywordStats(@Param("date") int date, @Param("limit") int limit);
}
