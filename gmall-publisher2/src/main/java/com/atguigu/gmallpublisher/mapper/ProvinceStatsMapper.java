package com.atguigu.gmallpublisher.mapper;


import com.atguigu.gmallpublisher.bean.ProvinceStats;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface ProvinceStatsMapper {

    @Select("select province_name,sum(order_amount) order_amount from dws_trade_province_order_window where toYYYYMMDD(stt)=#{date} and province_name is not null and province_name !='' group by province_id,province_name")
    public List<ProvinceStats> selectProvinceStats(int date);

}
