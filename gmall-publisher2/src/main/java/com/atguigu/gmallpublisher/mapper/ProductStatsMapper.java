package com.atguigu.gmallpublisher.mapper;

import com.atguigu.gmallpublisher.bean.ProductStats;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.math.BigDecimal;
import java.util.List;

public interface ProductStatsMapper {

    @Select("select  spu_id,spu_name,sum(order_amount) order_amount,sum(dws_trade_user_spu_order_windoworder_ct) order_ct from product_stats where toYYYYMMDD(stt)=#{date} group by spu_id,spu_name having order_amount > 0 order by order_amount  desc limit #{limit} ")
    public List<ProductStats> getProductStatsGroupBySpu(@Param("date") int date, @Param("limit") int limit);

    @Select("select category3_id,category3_name,sum(order_amount) order_amount from dws_trade_user_spu_order_window where toYYYYMMDD(stt)=#{date} group by category3_id,category3_name having order_amount>0 order by order_amount desc limit #{limit}")
    public List<ProductStats> getProductStatsGroupByCategory3(@Param("date") int date, @Param("limit") int limit);

    @Select("select trademark_name,sum(order_amount) order_amount from dws_trade_user_spu_order_window where toYYYYMMDD(stt)=#{date} group by trademark_name having order_amount>0 order by order_amount desc limit #{limit}")
    public List<ProductStats> getProductStatsByTrademark(@Param("date") int date, @Param("limit") int limit);

    @Select("select sum(order_amount) order_amount from dws_trade_user_spu_order_window where toYYYYMMDD(stt)=#{date}")
    public BigDecimal getGMV(int date);

}
